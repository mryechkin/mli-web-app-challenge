# Application Challenge
#### Coding challenge for prospective candidates

## Instructions

1. The objective of this exercise is to create a runnable web application, to the below acceptance criteria
2. The data required is contained in the `/data` folder
3. You are free to use your language of choice, and any other supporting frameworks or libraries to assist you in the task. If you do so, please provide a brief reason why you selected the particular technology(ies) 
4. You are given full creative freedom to design the UI as you see fit to display the given data
5. Your application must be unit-tested
6. Provide any necessary instructions required to build and run your application
7. Instructions to share your code back with us will have been provided via email

## Acceptance Criteria

#### **SCENARIO 1 (Required):** The user launches your application for the first time

* GIVEN the user has their browser open
* AND the user has never loaded the application before
* WHEN the user loads the application URL
* THEN the user is taken to the application homepage, with a list of their accounts
* AND some data for each account


#### **SCENARIO 2 (Optional):** The user wants to view the transactions of a particular account

* GIVEN the user is viewing a list of accounts
* WHEN the user clicks on a specific account
* THEN the user is presented with a view containing a transaction history for the specified account


#### **SCENARIO 3 (Optional):** The user wants to transfer money between two accounts

* GIVEN the user is on the homepage
* AND the user fills out a form to transfer money
* WHEN the user clicks Transfer Money button
* THEN the specified amount is moved from source to destination accounts
* AND a confirmation message is displayed on the page